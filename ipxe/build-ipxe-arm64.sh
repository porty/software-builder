#!/bin/bash

if [ ! -d ipxe ] ; then
    echo "Cloning ipxe"
    git clone git://git.ipxe.org/ipxe.git
else
    echo "Already got ipxe, not cloning/updating"
fi

# CROSS_COMPILE=aarch64-linux-gnu- make bin-arm64-efi/ipxe.usb
docker build -t ipxe-build-debian .
docker run -it --rm \
    -v "$(pwd)":/work \
    -w /work/ipxe/src \
    -e CROSS_COMPILE=aarch64-linux-gnu- \
    ipxe-build-debian \
    make bin-arm64-efi/ipxe.usb
